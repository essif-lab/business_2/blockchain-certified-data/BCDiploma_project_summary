# Project Summary

For more information, please contact: vincent.langard@bcdiploma.com

![](https://www.bcdiploma.com/img/etherscan-banner.png)

## Summary

Blockchain Certified Data publishes and commercializes a digital credentials solution for institutions (academic, corporate and government): [BCdiploma](https://www.bcdiploma.com).

BCdiploma is a blockchain-based service for issuing 100% digital and secure credentials that has convinced over 100 universities in 19 countries.

## The challenge(s)

SSI will soon be a standard, as evidenced by the European EBSI project and others initiatives around the world (e.g. Digital Credentials Consortium). In fact, the challenge goes far beyond the case of diplomas : Self-Sovereign Identity (SSI) support will soon be a market requirement for Verifiable Credentials solutions. 

As a result, BCdiploma, which is very active in the French EBSI project (FR.EBSI, led by University of Lille), has decided to adapt its products and use the self-sovereign identity (SSI) aligned with ESSIF. However, beyond the technical implementation of the standards, how to design a usable solution without technical barriers ? More generally, how to ensure adoption of the standard ?

We believe that the main challenge to the adoption of dematerialization technologies is the lack of confidence in the durability of existing solutions, due to their centralized nature, proprietary format and lack of interoperability.

The other challenge is adoption by end users - namely students - who are not ready to adopt overly technical and non "turnkey” solutions, and want to retain full control over their data.

## Solution

To overcome these challenges, Blockchain Certified Data will develop and integrate an SSI-enabled enterprise wallet and a student wallet, including a verifier, into its TRL9 BCdiploma application, in production.

Integrating SSI technologies into an already decentralized solution like BCdiploma will provide a level of interoperability unique in the market, increasing institutional confidence in the sustainability and viability of the solution. Also, BCdiploma's expertise in the academic environment combined with the eSSIF framework's capabilities in terms of user sovereignty over their data will allow maximum adoption.

BCdiploma has chosen to take up this challenge in collaboration with the walt.id teams: we share a common vision of a future open source wallet, compliant with the new EU identity standards (ESSIF), easy to integrate and easy to use for all citizens. 

## Ecosystem

BCdiploma already has a significant ecosystem. Notably, we are deploying our solution in the first instance within the [French EBSI project (fr.EBSI)](https://www.bcdiploma.com/blog/digital-credentials-press-release-university-of-lille-project-2021-06-02), in collaboration with the University of Lille (80k students). This playground will be the first implementation of the SSI features we are developing.

In a second phase, [all the Institutions already using BCdiploma](https://www.bcdiploma.com/issuersList) will be able to use the solution: ESCP Europe, emlyon business school, Universities of Nantes, Montpellier, Bourgogne, University of New York in Prague, Arts et Metiers ParisTech, eCampusOntario, Mancosa University etc., are all potential users.

From a technical standpoint, we will be working with [Walt.id](https://walt.id) to implement the self-sovereign identity mechanisms in BCdiploma and implement them in our solution, and we will be collaborating with [Yes.com](https://yes.com) to integrate their DID authentication solution into our portal.
<p align="center">
  <a href="https://www.bcdiploma.com/">
  <img src="https://www.bcdiploma.com/img/bcdiploma-no-baseline-200x200.png" alt="BCdiploma logo"/>
  </a>
</p>
